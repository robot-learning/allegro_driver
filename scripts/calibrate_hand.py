#!/usr/bin/env python
import rospy
from std_msgs.msg import Char
from sensor_msgs.msg import JointState
import matplotlib.pyplot as plt
import copy
import sys
import numpy as np
import time
_DISPLAY_RATE = 50
class JointTester:

    def __init__(self, node_name='joint_tester', listen_prefix='/allegro_hand_right', listen=False, publish_prefix='/allegro_hand_right'):
        rospy.init_node(node_name)

        if listen:
            rospy.Subscriber(listen_prefix+'/joint_states', JointState,
                             self.joint_state_cb)
            self.joint_state=JointState()

        self.allegro_joint_cmd_pub = rospy.Publisher('/allegro_hand_right'+'/joint_cmd',
                                                     JointState, queue_size=5)

        #self.allegro_ctrl_type_pub = rospy.Publisher(publish_prefix+'/allegro_hand_right/control_type', Char,
        #                                     queue_size=5)
        self.run_rate=rospy.Rate(100)
        self.j_vel=[]
    def send_joint_command(self,pos):
        run_rate = rospy.Rate(100)
        
        jc = JointState()
        jc.name = ['index_joint_0','index_joint_1','index_joint_2', 'index_joint_3',
                   'middle_joint_0','middle_joint_1','middle_joint_2', 'middle_joint_3',
                   'ring_joint_0','ring_joint_1','ring_joint_2', 'ring_joint_3',
                   'thumb_joint_0','thumb_joint_1','thumb_joint_2', 'thumb_joint_3']



        jc.position = pos
        for i in xrange(10):
            self.allegro_joint_cmd_pub.publish(jc)

    def joint_state_cb(self, cur_joint_state):
        self.joint_state=cur_joint_state
        self.j_vel.append(cur_joint_state.velocity[3])
        #print self.joint_state
        #if cur_joint_state.header.seq % _DISPLAY_RATE == 0:
        #    print cur_joint_state.name

    def run_allegro_test(self, control_type='p'):
        run_rate = rospy.Rate(50)
        #self.allegro_ctrl_type_pub.publish(Char(data=ord(control_type)))
        jc = JointState()
        jc.name = ['index_joint_0','index_joint_1','index_joint_2', 'index_joint_3',
                   'middle_joint_0','middle_joint_1','middle_joint_2', 'middle_joint_3',
                   'ring_joint_0','ring_joint_1','ring_joint_2', 'ring_joint_3',
                   'thumb_joint_0','thumb_joint_1','thumb_joint_2', 'thumb_joint_3']

        if(control_type=='p'):
            # TODO: set this to work for velocity
            jc.position = [ 0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0]

            delta_position = 0.015

            for i in xrange(100):
                self.allegro_joint_cmd_pub.publish(jc)

                for j in xrange(len(jc.name)):
                    jc.position[j] += delta_position
                run_rate.sleep()

            for i in xrange(100):
                self.allegro_joint_cmd_pub.publish(jc)
                for j in xrange(len(jc.name)):
                    jc.position[j] -= delta_position
                run_rate.sleep()

        if(control_type=='e'):
            # TODO: set this to work for velocity
            jc.effort = [ 0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0]

            
            delta_effort = 0.07
            f=[3]
            for i in xrange(25):
                self.allegro_joint_cmd_pub.publish(jc)
                for j in f:
                    jc.effort[j] = delta_effort
                run_rate.sleep()

            jc.effort = [ 0.0, 0.0, 0.0, 0.0,
                          0.0, 0.0, 0.0, 0.0,
                          0.0, 0.0, 0.0, 0.0,
                          0.0, 0.0, 0.0, 0.0]

            for i in xrange(25):
                self.allegro_joint_cmd_pub.publish(jc)
                for j in f:
                    jc.effort[j] =-1.0* delta_effort
                run_rate.sleep()
            jc.effort = [ 0.0, 0.0, 0.0, 0.0,
                          0.0, 0.0, 0.0, 0.0,
                          0.0, 0.0, 0.0, 0.0,
                          0.0, 0.0, 0.0, 0.0]
            self.allegro_joint_cmd_pub.publish(jc)
        if(control_type=='d'):
            # Testing effort control on thumb and position control on other fingers.

            jc.effort = [ 0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0,
                            0.1, 0.1, 0.1, 0.1]
            jc.position = [ 0.2, 0.2, 0.2, 0.2,
                            0.2, 0.2, 0.2, 0.2,
                            0.2, 0.2, 0.2, 0.2,
                            0.0, 0.0, 0.0, 0.0]

            for i in xrange(100):
                self.allegro_joint_cmd_pub.publish(jc)
                run_rate.sleep()

    def oscillate_joint(self,joint):
        j_pos=[]
        tau=[]
        j_vel=[]
        #self.allegro_ctrl_type_pub.publish(Char(data=ord(control_type)))
        jc = JointState()
        jc.name = ['index_joint_0','index_joint_1','index_joint_2', 'index_joint_3',
                   'middle_joint_0','middle_joint_1','middle_joint_2', 'middle_joint_3',
                   'ring_joint_0','ring_joint_1','ring_joint_2', 'ring_joint_3',
                   'thumb_joint_0','thumb_joint_1','thumb_joint_2', 'thumb_joint_3']
                
        jc.effort = [ 0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0]

        t=40
        delta_effort = 0.1
        f=[joint]
        for i in xrange(t):
            j_pos.append(self.joint_state.position[joint])
            j_vel.append(self.joint_state.velocity[joint])
            tau.append(delta_effort)
            self.allegro_joint_cmd_pub.publish(jc)
            for j in f:
                jc.effort[j] = delta_effort
            self.run_rate.sleep()
            
        jc.effort = [ 0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0]

        for i in xrange(t):
            j_pos.append(self.joint_state.position[joint])
            j_vel.append(self.joint_state.velocity[joint])
            tau.append(-1.0*delta_effort)
            self.allegro_joint_cmd_pub.publish(jc)
            for j in f:
                jc.effort[j] =-1.0* delta_effort
            self.run_rate.sleep()
            
        return j_pos,j_vel,tau

    def position_step(self,joint):
        j_pos=[]
        tau=[]
        j_vel=[]
        time_=[]
        start = time.time()
        #self.allegro_ctrl_type_pub.publish(Char(data=ord(control_type)))
        jc = JointState()
        jc.name = ['index_joint_0','index_joint_1','index_joint_2', 'index_joint_3',
                   'middle_joint_0','middle_joint_1','middle_joint_2', 'middle_joint_3',
                   'ring_joint_0','ring_joint_1','ring_joint_2', 'ring_joint_3',
                   'thumb_joint_0','thumb_joint_1','thumb_joint_2', 'thumb_joint_3']
                
        jc.position = np.ones(16)*-10.0
        t=150
        jc.position[joint]=0.5
        
        for i in xrange(t):
            j_pos.append(self.joint_state.position[joint])
            j_vel.append(self.joint_state.velocity[joint])
            tau.append(self.joint_state.effort[joint])
            time_.append(time.time()-start)
            self.allegro_joint_cmd_pub.publish(jc)
            self.run_rate.sleep()

        '''

        jc.position[joint]=0.0
        
        for i in xrange(t):
            j_pos.append(self.joint_state.position[joint])
            j_vel.append(self.joint_state.velocity[joint])
            tau.append(self.joint_state.effort[joint])
            time_.append(time.time()-start)
            self.allegro_joint_cmd_pub.publish(jc)
            self.run_rate.sleep()
        '''
        return j_pos,j_vel,tau,time_

    def effort_test(self,joint):
        j_pos=[]
        tau=[]
        j_vel=[]
        for i in range(5):
            j_p,j_v,t=self.oscillate_joint(joint)
            j_pos.extend(j_p)
            j_vel.extend(j_v)
            tau.extend(t)
            
        #self.allegro_ctrl_type_pub.publish(Char(data=ord(control_type)))
        jc = JointState()
        jc.name = ['index_joint_0','index_joint_1','index_joint_2', 'index_joint_3',
                   'middle_joint_0','middle_joint_1','middle_joint_2', 'middle_joint_3',
                   'ring_joint_0','ring_joint_1','ring_joint_2', 'ring_joint_3',
                   'thumb_joint_0','thumb_joint_1','thumb_joint_2', 'thumb_joint_3']
        jc.effort = [ 0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0]
        self.allegro_joint_cmd_pub.publish(jc)
        return j_pos,j_vel,tau
    
    def step_response(self,joint):
        
        j_pos=[]
        tau=[]
        j_vel=[]
        time_arr=[]
        for i in range(1):
            j_p,j_v,t,time_=self.position_step(joint)
            j_pos.extend(j_p)
            j_vel.extend(j_v)
            tau.extend(t)
            time_arr.extend(time_)
        
        #self.allegro_ctrl_type_pub.publish(Char(data=ord(control_type)))
        jc = JointState()
        jc.name = ['index_joint_0','index_joint_1','index_joint_2', 'index_joint_3',
                   'middle_joint_0','middle_joint_1','middle_joint_2', 'middle_joint_3',
                   'ring_joint_0','ring_joint_1','ring_joint_2', 'ring_joint_3',
                   'thumb_joint_0','thumb_joint_1','thumb_joint_2', 'thumb_joint_3']
        jc.effort = [ 0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0]
        jc.position=np.ones(16)*-10.0
        self.allegro_joint_cmd_pub.publish(jc)
        return j_pos,j_vel,tau,time_arr

    def grasp_response(self):
        jc = JointState()
        jc.name = ['index_joint_0','index_joint_1','index_joint_2', 'index_joint_3',
                   'middle_joint_0','middle_joint_1','middle_joint_2', 'middle_joint_3',
                   'ring_joint_0','ring_joint_1','ring_joint_2', 'ring_joint_3',
                   'thumb_joint_0','thumb_joint_1','thumb_joint_2', 'thumb_joint_3']
        jc.effort = [ 0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0]

        jc.position=np.ones(16)*-10.0
        jc.position[0]=0.2
        jc.position[1]=0.5
        jc.position[2]=0.5
        jc.position[3]=0.5

        jc.position[4]=0.2
        jc.position[5]=0.5
        jc.position[6]=0.5
        jc.position[7]=0.5

        jc.position[8]=0.2
        jc.position[9]=0.5
        jc.position[10]=0.5
        jc.position[11]=0.5

        jc.position[12]=1.2
        jc.position[13]=0.5
        jc.position[14]=0.5
        jc.position[15]=0.5

        
        
        #jc.position = [ 0.2, 0.2, 0.2, 0.2,
        #                0.2, 0.2, 0.2, 0.2,
        #                0.2, 0.2, 0.2, 0.2,
        #                1.5, 0.0, 0.0, 0.0]

        for i in xrange(40):
            self.allegro_joint_cmd_pub.publish(jc)
            self.run_rate.sleep()
        raw_input('grav?')
        # Back to grav comp
        jc.position=np.ones(16)*-10.0
        self.allegro_joint_cmd_pub.publish(jc)
        
def test_allegro_position_control(listen=False, publish_prefix='/allegro_hand_right', listen_prefix=''):

    jt = JointTester(listen=listen, listen_prefix=listen_prefix, publish_prefix=publish_prefix)
    rospy.loginfo('Running allegro control test')
    jt.run_allegro_test('p')

    
def test_allegro_effort_control(listen=False, publish_prefix='', listen_prefix=''):
    jt = JointTester(listen=listen, listen_prefix='', publish_prefix=publish_prefix)
    jt.run_allegro_test('e')

def sensor_plot(joint, listen=True, publish_prefix='', listen_prefix='/allegro_hand_right'):
    # Move joint and plot sensor feedback
    jt = JointTester(listen=listen, listen_prefix=listen_prefix, publish_prefix=publish_prefix)
    jt.run_rate.sleep()
    while(not rospy.is_shutdown() and len(jt.joint_state.position)<1):
        jt.run_rate.sleep()

    jt.grasp_response()
    exit()
    # effort:
    #j_pos,j_vel,tau=jt.effort_test(joint)
    # position:
    #j_pos,j_vel,tau,time_arr=jt.step_response(joint)
    
    plot_j(j_pos,j_vel,tau,time_arr)
    
    #plt.figure(2)
    #tot_jvel=copy.deepcopy(jt.j_vel)
    #plt.plot(tot_jvel, 'g',label='Joint Velocity')
    #plt.show()
def plot_j(j_pos,j_vel,tau,t):
    fig, axes = plt.subplots(nrows=2, ncols=1)
    plt.setp(axes, xticks=np.arange(0, max(t), 0.05))
  
    #plt.subplot(311)
    axes[0].set_title('Joint Position')
    axes[0].plot(t,j_pos,'r',label='Joint Position')
    axes[0].grid()
    
    '''
    axes[1].set_title('Joint Velocity')
    axes[1].plot(t,j_vel, 'g',label='Joint Velocity')
    axes[1].grid()
    '''
    axes[1].set_title('Joint Torque input')
    axes[1].plot(t,tau, 'b',label='Torque input')
    axes[1].grid()
    
    plt.show()
    
if __name__=='__main__':
    sensor_plot(int(sys.argv[1]))
    
