import rospy
import tf
import PyKDL
import numpy as np
from geometry_msgs.msg import PoseStamped
class gVector:
    def __init__(self,node_name='g_vector_publish_node',rate=100):
        rospy.init_node(node_name)
        self.rate=rospy.Rate(rate)
        self.listener=tf.TransformListener()
        self.got_tf=False
        self.pose_pub=rospy.Publisher('/g_vector',PoseStamped,queue_size=1)
        

    def tf_find_transform(self,target_frame='base_link',base_frame='palm_link'):
        while(not self.got_tf):
            try:
                (trans_2,rot_2)= self.listener.lookupTransform(base_frame, target_frame, rospy.Time(0))
                final_pose=np.zeros(6)
                final_pose[0:3]=trans_2
                final_pose[3:6]=tf.transformations.euler_from_quaternion(rot_2)
                self.got_tf=True
            except:
                continue
        return final_pose

    def g_vector(self):
        g_base=np.matrix([0.0,0.0,0.0,0.0]).T
        T_g=np.eye(4)
        R_=PyKDL.Rotation()
        R_=R_.RPY(0,3.14/2.0,0)# This is the g_vector in the base_link frame
        
        for k in range(3):
            for l in range(3):
                T_g[k,l]=R_[k,l]
        while not rospy.is_shutdown():
            pose_=self.tf_find_transform()
            if(self.got_tf):
                R=PyKDL.Rotation()
                R=R.RPY(pose_[3],pose_[4],pose_[5])
                T=np.eye(4)
                for k in range(3):
                    for l in range(3):
                        T[k,l]=R[k,l]
                T[0:3,3]=pose_[0:3]
                T=np.matrix(T)

                g_T=(T*T_g)

                R=PyKDL.Rotation()
               
                for k in range(3):
                    for l in range(3):
                        R[k,l]=g_T[k,l]
                print R.GetRPY()
                qua= R.GetQuaternion()
                new_pose=PoseStamped()
                new_pose.header.frame_id='palm_link'
                new_pose.pose.position.x=0.0
                new_pose.pose.position.y=0.0
                new_pose.pose.position.z=0.0
                
                new_pose.pose.orientation.x=qua[0]
                new_pose.pose.orientation.y=qua[1]
                new_pose.pose.orientation.z=qua[2]

                new_pose.pose.orientation.w=qua[3]
                self.pose_pub.publish(new_pose)
                
                '''
                # Orientation:
                x=np.arccos(np.dot([1.0,0.0,0.0],g_pose))
                y=np.arccos(np.dot([0.0,1.0,0.0],g_pose))
                z=np.arccos(np.dot([0.0,0.0,1.0],g_pose))
                qua=tf.transformations.quaternion_from_euler(x,y,z)
                '''
                self.got_tf=False
            self.rate.sleep()

gClass=gVector()
gClass.g_vector()
