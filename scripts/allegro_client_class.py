# This file shows how to use clients to connect to the available services

import rospy
from sensor_msgs.msg import JointState
from allegro_driver.srv import *
import numpy as np

class allegroClient:
    def __init__(self,init_node=False):
        
        if(init_node):
            rospy.init_node('allegro_client_node')

    def read_joints(self):
        # read current joint state
        rospy.wait_for_service('allegro/current_joint_state')
        try:
            j_state=rospy.ServiceProxy('allegro/current_joint_state',AllegroJointState)
            resp1=j_state()
            return resp1.joint_state
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e    
    def send_pos_cmd_arr(self,j_arr):
        rospy.wait_for_service('allegro/send_pos_cmd_arr')
        try:
            j_cmd=rospy.ServiceProxy('allegro/send_pos_cmd_arr',AllegroPositionCommandArray)
            resp1=j_cmd(j_arr)
            return resp1.sent_cmd
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
            
    def grav_mode(self):
        rospy.wait_for_service('allegro/grav_comp_mode')
        try:
            j_cmd=rospy.ServiceProxy('allegro/grav_comp_mode',AllegroGravCompMode)
            resp1=j_cmd()
            return resp1.enabled_mode
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
