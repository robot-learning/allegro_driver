#!/usr/bin/env python
# This file launches servers for ros services to make comms with allegro easier.
import rospy
import simple_allegro_ros_api
from allegro_driver.srv import *
import numpy as np

class allegroSrv:
    def __init__(self):
        # initialize services
        print 'initializing allegro ros services'
        rospy.init_node('allegro_services_node')
        self.hand=simple_allegro_ros_api.handAPI()
    def read_joints(self,req):
        # read current joint state
        jnt_state=self.hand.joint_state
        return jnt_state
    
    def send_pos_cmd(self,req):
        self.hand.publish_joints(req.joint_angles)
        return True
    
    def grav_mode(self,req):
        # TODO: Add finger array for grav compensation
        j_arr=np.ones(16)*-10.0
        self.hand.publish_joints(j_arr)
        return True
    
    def init_services(self):

        jnt_read=rospy.Service('allegro/current_joint_state',AllegroJointState,self.read_joints)
        pos_cmd_arr=rospy.Service('allegro/send_pos_cmd_arr',AllegroPositionCommandArray,self.send_pos_cmd)
        grav_mode=rospy.Service('allegro/grav_comp_mode',AllegroGravCompMode,self.grav_mode)
        rospy.spin()
        
if __name__=="__main__":
    allegro_srv=allegroSrv()
    allegro_srv.init_services()
    

