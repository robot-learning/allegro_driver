# This class wraps allegro joint states and publishing methods to allow easy use in other packages.
import rospy
from sensor_msgs.msg import JointState
import numpy as np
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
import sys
from rospkg import RosPack
import copy
# Import robot hand api
rp=RosPack()
rp.list()
path=rp.get_path('ll4ma_kdl')+'/scripts'
sys.path.insert(0,path)
from kdl_allegro_model import allegroKinematics
class handAPI:
    def __init__(self,node_name='allegro_node',publish_prefix='/allegro_hand_right',listen_prefix='/allegro_hand_right',rate=100.0,init_node=False):
        if(init_node):
            rospy.init_node(node_name)
            
        
        self.robot_joint_cmd_pub=rospy.Publisher(publish_prefix+'/joint_cmd',
                                                     JointState, queue_size=100)

        rospy.Subscriber(listen_prefix+'/joint_states',JointState,self.joint_state_cb)

        #print 'Initialized robot node'
        self.loop_rate=rospy.Rate(rate)

        jc=JointState()
        jc.name = ['index_joint_0','index_joint_1','index_joint_2', 'index_joint_3',
                   'middle_joint_0','middle_joint_1','middle_joint_2', 'middle_joint_3',
                   'ring_joint_0','ring_joint_1','ring_joint_2', 'ring_joint_3',
                   'thumb_joint_0','thumb_joint_1','thumb_joint_2', 'thumb_joint_3']

        '''
        jc.name=["joint_0", "joint_1", "joint_2", "joint_3",
                "joint_4", "joint_5", "joint_6", "joint_7",
                "joint_8", "joint_9", "joint_10", "joint_11",
                "joint_12", "joint_13", "joint_14", "joint_15"]
        '''
        jc.position=[ 0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0,
                      0.0, 0.0, 0.0, 0.0]
        self.jc=jc
        
        # robot kdl:
        self.robot_kdl=allegroKinematics()
        self.DOF=4
        self.got_state=False
        
    def joint_state_cb(self,joint_state):
        self.joint_state=joint_state
        self.got_state=True

    def get_joint_state(self):
        while(not self.got_state):
            self.loop_rate.sleep()
        return self.joint_state
    def engage_grav_comp(self):
        # Engage grav compensation mode
        # WARNING!! Only works with LL4MA lab driver, do not try with simlab driver!!!!
        
        self.jc.position=np.ones(16)*-10.0
        self.jc.effort=np.ones(16)*0.0
        while not rospy.is_shutdown():
            for i in range(20):
                self.robot_joint_cmd_pub.publish(self.jc)
                self.loop_rate.sleep()
            break
    
    def publish_joints(self,joint_pos_arr):
        self.jc.position=joint_pos_arr
        while not rospy.is_shutdown():
            for i in range(20):
                self.robot_joint_cmd_pub.publish(self.jc)
                self.loop_rate.sleep()
            break

    def publish_joints_slow(self,joint_pos_arr):
        while not rospy.is_shutdown():
            for i in range(200):
                self.jc.position=np.array(self.joint_state.position)+(joint_pos_arr-np.array(self.joint_state.position))*float(i)/200.0
                self.robot_joint_cmd_pub.publish(self.jc)
                self.loop_rate.sleep()
            break
    def publish_joints_slow_finger(self,joint_pos_arr,f_idx):
        while not rospy.is_shutdown():
            for i in range(200):
                new_cmd=np.array(self.joint_state.position)+(joint_pos_arr-np.array(self.joint_state.position))*float(i)/200.0
                self.jc.position=copy.deepcopy(joint_pos_arr)
                self.jc.position[f_idx*4:f_idx*4+4]=new_cmd[f_idx*4:f_idx*4+4]
                self.robot_joint_cmd_pub.publish(self.jc)
                self.loop_rate.sleep()
            break

    def publish_joints_ctrl(self,joint_pos_arr):
        # This assumes there is an external loop rate setup and sends only one command
        self.jc.position=joint_pos_arr
        self.robot_joint_cmd_pub.publish(self.jc)

    def publish_joint_arr(self,u_arr):
        while not rospy.is_shutdown():
            for i in range(len(u_arr)):
                self.jc.position=u_arr[i]
                self.robot_joint_cmd_pub.publish(self.jc)
                self.loop_rate.sleep()
            break

    def publish_joint_traj(self,jtraj,dt=1):
        u_arr=self.interpolate_joint_traj(jtraj,dt)
        while not rospy.is_shutdown():
            for i in range(len(u_arr)):
                self.jc.position=u_arr[i]
                self.robot_joint_cmd_pub.publish(self.jc)
                self.loop_rate.sleep()
            break

    def interpolate_joint_traj(self,jtraj,step=10):
        pos_array=np.zeros((len(jtraj.points),16))
        for i in range(len(jtraj.points)):
            pos_array[i,:]=jtraj.points[i].positions
        t=len(pos_array)-1
        T=t*step
        dt=T/t
        new_jtraj=JointTrajectory()
        
        new_traj=np.zeros((T,16))
        for k in range(len(pos_array[0,:])):
            new_traj[0:dt,k]=np.linspace(pos_array[0,k],pos_array[1,k],dt)
        
        for i in range(1,len(pos_array)-1):
            for k in range(len(pos_array[i,:])):
                new_traj[i*dt-1:i*dt+dt,k]=np.linspace(pos_array[i,k],pos_array[i+1,k],dt+1)

        for i in range(T):
            tj_pt=JointTrajectoryPoint()
            tj_pt.positions=np.ravel(new_traj[i,:])
            new_jtraj.points.append(tj_pt)
        return new_traj,new_jtraj
            
    def vel_servo(self,thresh,delta_v_msg,joints):

        curr_pos=np.array(self.joint_state.position)
        delta_v=delta_v_msg
        grasped=False
        contact=[False for i in range(len(joints))]
        for j in range(20):
            for i in joints:
                curr_pos[i]+=delta_v
                self.jc.position=curr_pos
            self.robot_joint_cmd_pub.publish(self.jc)
            self.loop_rate.sleep()

        while not grasped:
            curr_vel=self.joint_state.velocity
            for i in range(len(joints)):
                if(curr_vel[joints[i]]>thresh):
                    curr_pos[joints[i]]+=delta_v
                else:
                    contact[i]=True
            self.jc.position=curr_pos
            self.robot_joint_cmd_pub.publish(self.jc)
            self.loop_rate.sleep()
            if(all(contact)):
                grasped=True
        return grasped
    
    def move_delta_q(self,delta_q,joints):
        curr_pos=np.array(self.joint_state.position)
        for i in joints:
            curr_pos[i]+=delta_q
            self.jc.position=curr_pos
        for j in range(2):
            #self.robot_joint_cmd_pub.publish(self.jc)
            self.loop_rate.sleep()
        return True
    
    def move_delta_q(self,delta_q_arr):
        curr_pos=np.array(self.joint_state.position)
        curr_pos+=delta_q_arr
        self.jc.position=curr_pos
        for j in range(2):
            self.robot_joint_cmd_pub.publish(self.jc)
            self.loop_rate.sleep()
        return True

    def ftip_force(self,finger):
        # Returns the force at the end-effector using torques+ jacobian
        self.get_joint_state()
        # get current joint positions and effort
        q=self.joint_state.position[finger*self.DOF:finger*self.DOF+self.DOF]

        q_tau=self.joint_state.effort[finger*self.DOF:finger*self.DOF+self.DOF]

        # Compute the force at the end-effector:
        J=self.robot_kdl.jacobian(q,finger)
        tip_force=np.ravel(J*np.matrix(q_tau).T)

        return tip_force
    
    def compute_vel_force(self,finger,des_force,k_p=np.eye(3)*1000.0):
        self.get_joint_state()
        q=self.joint_state.position[finger*self.DOF:finger*self.DOF+self.DOF]
        J=self.robot_kdl.jacobian(q,finger)
        
        q_dot_des=J.T*k_p*J*J.T*np.matrix(des_force).T

        return np.ravel(q_dot_des)
    
    def publish_traj(self,jtraj):
        while not rospy.is_shutdown():
            for i in range(jtraj.points):
                js=JointState()
                js.position=jtraj.points[i].positions
                js.velocity=jtraj.points[i].velocities
                self.robot_joint_cmd_pub.publish(self.jc)
                self.loop_rate.sleep()
            break
        
    '''
    def grasp_impedance_mode(self):
    '''
        
    '''
    def tighten_grasp(self,joints,robot_model,p_T_o,vel_offset):
        u=np.array(joints).copy()
        #Moves the fingertips closer to the object centroid using the Jacobian:
        for i in robot_model.fingers:
            J=robot_model.jacobian(joints[i*4:i*4+4],i)
            vel_vect=-(p_T_o*(np.matrix(vel_offset[i]).T))[0:3] #velocity direction
            u[i*4:i*4+4]=u[i*4:i*4+4]+(J.T*vel_vect*10.0).T 
        return u
    
    
    def tighten_traj(self,joint_traj,robot_model,vel_gains,f_T_O,fing_T_O):
        new_traj=np.zeros((len(joint_traj),len(joint_traj[0])))
        for i in range(len(joint_traj)):
            # Find feed-forward object pose:
            p_T_thumb=robot_model.end_effector_pose(joint_traj[i][12:16],3)
            p_T_O=np.matrix(p_T_thumb)*np.matrix(f_T_O) # Palm- thumb tf * thumb- object tf
            # Compute velocity offset
            vel_offset=np.zeros((4,4))
            for j in range(len(fing_T_O)):
                vel_offset[j,0:3]=vel_gains*fing_T_O[j]
            new_joints=self.tighten_grasp(joint_traj[i],robot_model,p_T_O,vel_offset)
            new_traj[i]=new_joints.copy()
        return new_traj
    '''
    '''
    def publish_joint(self,joint_pos_arr):
        self.jc.position=joint_pos_arr
        #while not rospy.is_shutdown():
        for i in range(1):
            self.robot_joint_cmd_pub.publish(self.jc)
            self.loop_rate.sleep()
            #break
    def publish_joint_trajectory(self,joint_pos_traj):
        while not rospy.is_shutdown():
            for i in range(len(joint_pos_traj)):
                self.jc.position=joint_pos_traj[i]
                self.robot_joint_cmd_pub.publish(self.jc)
                self.loop_rate.sleep()
            break
        #print 'Finished running trajectory'
    '''
        

    '''
    def publish_preshape(self,joint_pos_arr):
        self.jc.position=joint_pos_arr
        while not rospy.is_shutdown():
            for i in range(20):
                self.robot_joint_cmd_pub.publish(self.jc)
                self.loop_rate.sleep()
            break
    '''
