#!/usr/bin/env python
import rospy
from std_msgs.msg import Char
from sensor_msgs.msg import JointState


_DISPLAY_RATE = 50
class JointTester:

    def __init__(self, node_name='joint_tester', listen_prefix='', listen=False, publish_prefix='/allegro_hand_right'):
        rospy.init_node(node_name)

        if listen:
            rospy.Subscriber(listen_prefix+'/joint_states', JointState,
                             self.joint_state_cb)

        self.allegro_joint_cmd_pub = rospy.Publisher('/allegro_hand_right'+'/joint_cmd',
                                                     JointState, queue_size=5)

        #self.allegro_ctrl_type_pub = rospy.Publisher(publish_prefix+'/allegro_hand_right/control_type', Char,
        #                                     queue_size=5)
        self.run_rate=rospy.Rate(100)
        
    def send_joint_command(self,pos):
        run_rate = rospy.Rate(100)
        
        jc = JointState()
        jc.name = ['index_joint_0','index_joint_1','index_joint_2', 'index_joint_3',
                   'middle_joint_0','middle_joint_1','middle_joint_2', 'middle_joint_3',
                   'ring_joint_0','ring_joint_1','ring_joint_2', 'ring_joint_3',
                   'thumb_joint_0','thumb_joint_1','thumb_joint_2', 'thumb_joint_3']



        jc.position = pos
        for i in xrange(10):
            self.allegro_joint_cmd_pub.publish(jc)

    def joint_state_cb(self, cur_joint_state):
        if cur_joint_state.header.seq % _DISPLAY_RATE == 0:
            print cur_joint_state.name

    def run_allegro_test(self, control_type='p'):
        run_rate = rospy.Rate(50)
        #self.allegro_ctrl_type_pub.publish(Char(data=ord(control_type)))
        jc = JointState()
        jc.name = ['index_joint_0','index_joint_1','index_joint_2', 'index_joint_3',
                   'middle_joint_0','middle_joint_1','middle_joint_2', 'middle_joint_3',
                   'ring_joint_0','ring_joint_1','ring_joint_2', 'ring_joint_3',
                   'thumb_joint_0','thumb_joint_1','thumb_joint_2', 'thumb_joint_3']

        if(control_type=='p'):
            # TODO: set this to work for velocity
            jc.position = [ 0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0]

            delta_position = 0.015

            for i in xrange(100):
                self.allegro_joint_cmd_pub.publish(jc)

                for j in xrange(len(jc.name)):
                    jc.position[j] += delta_position
                run_rate.sleep()

            for i in xrange(100):
                self.allegro_joint_cmd_pub.publish(jc)
                for j in xrange(len(jc.name)):
                    jc.position[j] -= delta_position
                run_rate.sleep()

        if(control_type=='e'):
            # TODO: set this to work for velocity
            jc.effort = [ 0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0]

            
            delta_effort = 0.03

            for i in xrange(100):
                self.allegro_joint_cmd_pub.publish(jc)
                #for j in xrange(len(jc.name)):
                
                jc.effort[3] = delta_effort
                run_rate.sleep()

            jc.effort = [ 0.0, 0.0, 0.0, 0.0,
                          0.0, 0.0, 0.0, 0.0,
                          0.0, 0.0, 0.0, 0.0,
                          0.0, 0.0, 0.0, 0.0]

            for i in xrange(100):
                self.allegro_joint_cmd_pub.publish(jc)
                #for j in xrange(len(jc.name)):
                jc.effort[3] =-1.0* delta_effort
                run_rate.sleep()

        if(control_type=='d'):
            # Testing effort control on thumb and position control on other fingers.

            jc.effort = [ 0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0,
                            0.1, 0.1, 0.1, 0.1]
            jc.position = [ 0.2, 0.2, 0.2, 0.2,
                            0.2, 0.2, 0.2, 0.2,
                            0.2, 0.2, 0.2, 0.2,
                            0.0, 0.0, 0.0, 0.0]

            for i in xrange(100):
                self.allegro_joint_cmd_pub.publish(jc)
                run_rate.sleep()
                

def test_allegro_position_control(listen=False, publish_prefix='/allegro_hand_right', listen_prefix=''):

    jt = JointTester(listen=listen, listen_prefix=listen_prefix, publish_prefix=publish_prefix)
    rospy.loginfo('Running allegro control test')
    jt.run_allegro_test('p')

    
def test_allegro_effort_control(listen=False, publish_prefix='', listen_prefix=''):
    jt = JointTester(listen=listen, listen_prefix='', publish_prefix=publish_prefix)
    jt.run_allegro_test('e')

    
def test_lbr4_go_home():
    jt = JointTester(publish_prefix='/lbr4')
    jc = JointState()
    jc.name = ['lbr4_j0','lbr4_j1','lbr4_j2','lbr4_j3','lbr4_j4','lbr4_j5','lbr4_j6']
    # TODO: set this to work for velocity and effort
    jc.position = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.1]
    jt.joint_cmd_pub.publish(jc)

    
if __name__=='__main__':
    test_allegro_effort_control()

