/*
 * Copyright (C) 2017  Balakumar Sundaralingam, LL4MA lab
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// This file reads the current state of the allegro hand and also sends joint commands
#include "canAPI.h"
#include "rDeviceAllegroHandCANDef.h"
#include <stdio.h>
#include <stdlib.h>
#include <boost/thread.hpp>
#include "allegro_offsets.h"
#include <chrono>
#include <ros/ros.h>
#include "Eigen/Dense"

namespace allegro_driver
{
  class allegroComms
  {
  public:
    allegroComms(float);// initialization

    bool connect(bool cmd_robot);// initializes CAN comms and connects to robot
    
    void ioThreadProc();
      
    void disconnect();// disconnects robot and closes CAN comms.

    //command_mode(bool);// initializes command mode

    bool get_state(Eigen::VectorXd &q, Eigen::VectorXd &q_dot, Eigen::VectorXd &tau);// return the current robot state(position,velocity,torques)

    bool get_encoders();
    void send_torques(const Eigen::VectorXd &tau);// Sends torques to the robot
  private:
    bool torques_to_robot;
    float MAX_CURRENT;
    
    int CAN_Ch;

    bool ioThreadRun;
    boost::thread* thr;


    int recvNum;

    AllegroHand_DeviceMemory_t vars;

    // robot states:
    
    vector<double> _q;
    Eigen::VectorXd _q_filtered;
    
    vector<double> _q_old;
    vector<double> _q_old_filtered;
    vector<double> _q_dot;
    vector<double> _q_dot_old;
    Eigen::VectorXd _q_dot_filtered;
    
    double sample_rate;
    double write_rate;
    
    vector<double> _tau;
    vector<double> _cur_des;
    Eigen::VectorXd _tau_des;
    std::chrono::time_point<std::chrono::system_clock> time_now, end_time,write_time;
    vector<std::chrono::time_point<std::chrono::system_clock>> dt_clocks;
    //
    double dt,wt;
    ros::Time tstart;
    ros::Time tnow;
    unsigned char data_return;
    bool got_new_can_message;
    bool got_first_message;
    vector<bool> got_finger_position;
  };
}
