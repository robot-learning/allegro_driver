const bool	RIGHT_HAND = true;
const int	HAND_VERSION = 3;

const double tau_cov_const_v2 = 800.0; // 800.0 for SAH020xxxxx
const double tau_cov_const_v3 = 3500.0; // 1200.0 for SAH030xxxxx

const double enc_dir[MAX_DOF] = { // SAH030xxxxx
  1.0, 1.0, 1.0, 1.0,
  1.0, 1.0, 1.0, 1.0,
  1.0, 1.0, 1.0, 1.0,
  1.0, 1.0, 1.0, 1.0
};
const double motor_dir[MAX_DOF] = { // SAH030xxxxx
  1.0, 1.0, 1.0, 1.0,
  1.0, 1.0, 1.0, 1.0,
  1.0, 1.0, 1.0, 1.0,
  1.0, 1.0, 1.0, 1.0
};
const int enc_offset[MAX_DOF] = { // SAH030C033R
  1502,-2000,5281,4960,
  2644,-2000,4100,5223,
  5071,-2600,7941,6149,
  5000,3500,-3182,-5484
};
/*
const int enc_offset[MAX_DOF] = { // SAH030C033R
  1502,-2981,4281,3960,
  2644,-2883,3247,4223,
  5071,-4108,7941,5149,
  5467,3329,-4182,-5484
};
*/
/*
const int enc_offset[MAX_DOF] = { // SAH030C063R
  3337,-1621,-13727,1417,
  -2033,-12361,-14310,-669,
  -446,-3042,-4615,-14538,
  7128,1835,3,-11988
};
*/
const double LPF_beta[MAX_DOF]={
  0.95,  0.95,  1.0,  0.95,
  0.95,  0.95,  0.98,  0.95,
  0.95,  0.95,  0.95,  0.95,
  0.95,  0.95,  0.95,  0.95,
  
};
