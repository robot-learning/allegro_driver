	________________________________________________________

		      ALLEGRO HAND DRIVER FOR ROS

	 Balakumar Sundaralingam, LL4MA Lab, University of Utah
	________________________________________________________


Table of Contents
_________________

1 Scope:
2 Dependencies:
3 How to use this?
4 Future work
5 Implementation Details
6 Known Bugs


1 Scope:
========

  Package interfaces with the Allegro hand and has different controllers
  for position, velocity and torque based control.


2 Dependencies:
===============

  1. ll4ma_kdl
  2. ll4ma_robots_description
  3. kdl
  4. ros-kinetic-moveit-ros-visualization



3 How to use this?
==================

3.1 Connecting to allegro:
~~~~~~~~~~~~~~~~~~~~~~~~~~

(check section 6 Known Bugs if something doesn't work)

Only one of three options can be run: 

  1. Check if all your connections are right and run the allegro hand in
     monitor mode: `roslaunch allegro_driver monitor_mode.launch
     BIOTAC:=true'
  2. To run robot in gravity compensation: `roslaunch allegro_driver
     grav_comp.launch BIOTAC:=true'
  3. To run PD joint position mode: `roslaunch allegro_driver
     tracking_controller.launch BIOTAC:=true'


3.2 ROS services:
~~~~~~~~~~~~~~~~~

  ROS services are also available. See `scripts/allegro_client_demo.py'
  for an example call to the different services.


3.3 API for sending/receiving data:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  Use `scripts/simple_allegro_ros_api.py' to send and recieve
  information.

  To send joint commands :
  1. If you want only track joint positions, send only position values
     through the jointstate msg.
  2. If you want only track joint torques, send only effort values
     through the jointstate msg.
  3. If you want to track both position and effort, send effort as 0.0
     for joints that you want to track as position.
  4. Send a position of -10.0 for joints to be in grav_comp mode.


4 Future work
=============

  + OROCOS integration for real-time control.(Latest pcan drivers have
    support for Xenomai kernel)


5 Implementation Details
========================

5.1 Joint Position control:
~~~~~~~~~~~~~~~~~~~~~~~~~~~

  PD control has poor accuracy, a PD control with gravity compensation
  is hence implemented.


5.2 Joint Torque control:
~~~~~~~~~~~~~~~~~~~~~~~~~


5.3 Impedance control:
~~~~~~~~~~~~~~~~~~~~~~

6 Known Bugs
~~~~~~~~~~~~~~~~~~~~~~

If you get:
```
<< CAN: Open Channel...
initCAN(): CAN_Initialize() failed with error 512
The driver is not loaded
****ERROR command_canopen 512 !!! 
```
it is because the hand is not connected to the expected USB BUS. To change the expected USB BUS, edit `src/allegro_comms/allegro_comms.cpp` line 62 to a different USB BUS.
