/*
 * Copyright (C) 2017  Balakumar Sundaralingam, LL4MA lab
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// This file connects to the allegro hand and runs an inverse dynamics controller.
#include "allegro_comms/allegro_comms.h"
#include "controllers/pd_parameters.h"
#include "controllers/kdl_controller.h"


#include <ros/ros.h>
#include <ros/console.h>
#include <sensor_msgs/JointState.h>

const int ARM_DOF = 7;
const int FINGER_DOF = 4;
const int TOTAL_FINGERS = 4;
const int HAND_DOF = FINGER_DOF * TOTAL_FINGERS; 

Eigen::VectorXd q_des, q_dot_des, q_dd_des;

bool got_des=false;
bool lbr4_state=false;
Eigen::VectorXd arm_q;

// Subscribe to joint states
void stateCallback(sensor_msgs::JointState joint_msg)
{
  for(int i=0; i<ARM_DOF; i++)
    {
      arm_q[i]=joint_msg.position[i];
    }
  lbr4_state=true;
}


void jntCallback(sensor_msgs::JointState j)
{
  vector<double> effort;
  vector<double> position;
  vector<double> velocity;
  effort=j.effort;
  position=j.position;
  velocity=j.velocity;
  
  q_des.setZero(HAND_DOF);
  q_dot_des.setZero(HAND_DOF);
  q_dd_des.setZero(HAND_DOF);

  if(!effort.empty())
  {
    for(int i=0; i<HAND_DOF; i++)
    {
      q_dd_des[i] = effort[i];
    }
  }
  if(!position.empty())
  {
    for(int i=0; i<HAND_DOF; i++)
    {
      q_des[i] = position[i];
    }
  }
  if(!velocity.empty())
  {
    for(int i=0; i<HAND_DOF; i++)
    {
      q_dot_des[i] = velocity[i];
    }
  }
  got_des=true;
}

int main(int argc, char** argv)
{
  ros::init(argc,argv,"allegro_inv_dynamic_node");
  ros::NodeHandle n;

  ros::Publisher st_pub=n.advertise<sensor_msgs::JointState>("allegro_hand_right/joint_states",1);
  ros::Subscriber cmd_sub=n.subscribe<sensor_msgs::JointState>("allegro_hand_right/joint_cmd",1,jntCallback);  
  ros::Subscriber lbr4_sub=n.subscribe("/iiwa/joint_states",1,stateCallback);

  // global variables
  q_des = Eigen::VectorXd::Zero(HAND_DOF);
  q_dot_des = Eigen::VectorXd::Zero(HAND_DOF);
  q_dd_des = Eigen::VectorXd::Zero(HAND_DOF);
  arm_q = Eigen::VectorXd::Zero(ARM_DOF);

  // hand joints 
  Eigen::VectorXd q = Eigen::VectorXd::Zero(HAND_DOF);
  Eigen::VectorXd q_dot = Eigen::VectorXd::Zero(HAND_DOF);

  // hand torques
  Eigen::VectorXd tau = Eigen::VectorXd::Zero(HAND_DOF);
  Eigen::VectorXd tau_force = Eigen::VectorXd::Zero(HAND_DOF);
  Eigen::VectorXd tau_pos = Eigen::VectorXd::Zero(HAND_DOF);

  allegro_driver::allegroComms robot(0.65);

  // create kdl controller instance:
  std::vector<double> g_vec={0.0,0.0,-9.8}; 
  double loop_rate=1000.0;
  ros::Rate r(loop_rate);
  allegroKDL kdl_comp(g_vec,loop_rate);

  kdl_comp.load_gains(K_p,K_d,traj_K_p,traj_K_d,traj_K_i,vel_K_d,max_tau_des,max_delta_q,max_q_vel);

  sensor_msgs::JointState curr_state;
  std::string names[]={"index_joint_0","index_joint_1","index_joint_2", "index_joint_3",
                   "middle_joint_0","middle_joint_1","middle_joint_2", "middle_joint_3",
                   "ring_joint_0","ring_joint_1","ring_joint_2", "ring_joint_3",
                   "thumb_joint_0","thumb_joint_1","thumb_joint_2", "thumb_joint_3"};
  
  
  std::vector<std::string> name_vect(names,names+sizeof(names)/sizeof(std::string));
  curr_state.name=name_vect;
  curr_state.position = std::vector<double>(HAND_DOF);
  curr_state.velocity = std::vector<double>(HAND_DOF);
  curr_state.effort = std::vector<double>(HAND_DOF);

  tau_force.resize(HAND_DOF);

  // urdf file location:
  string urdf_file = ros::package::getPath("ll4ma_robots_description");
  urdf_file.append("/robots/kuka-allegro-biotac.urdf");
  vector<string> ee_names={"index_biotac_tip","middle_biotac_tip","ring_biotac_tip","thumb_biotac_tip"}; 
  vector<string> base_names={"world","world","world","world"}; 

  // initialize kdl class:
  robotKDL lbr4_(urdf_file,base_names,ee_names,g_vec);
  for (int i = 0; i < TOTAL_FINGERS; i++)
  {
	  cout << "chain: " << std::to_string(i) << endl;
	  std::vector<std::string> joint_names = lbr4_.getJointNames(i);
	  for(int j = 0; j < joint_names.size(); j++)
	  {
		  cout << joint_names[j] << endl;
	  }
	  cout << endl;
  }

  Eigen::VectorXd tau_g = Eigen::VectorXd::Zero(HAND_DOF);
  Eigen::VectorXd temp_tau_g = Eigen::VectorXd::Zero(ARM_DOF + FINGER_DOF);
  Eigen::VectorXd temp_q = Eigen::VectorXd::Zero(ARM_DOF + FINGER_DOF);

  bool st=robot.connect(true);
  int iteration = 0;
  while(ros::ok() && st)
    {
      ros::spinOnce();
      if(lbr4_state != true)
      {
         ROS_INFO("waiting for LBR4 joint states..");
	 continue;
      }
      for (int i=0; i < HAND_DOF; i++)
      {
        tau_pos[i] = 0.0;
      }

      robot.get_state(q,q_dot,tau);

      // compute torques from controller:
      if(got_des)
      {
	// if no velocity desired, run PD on old q desired
        if(q_dot_des.sum() == 0)
        {
          kdl_comp.get_PD(q_des, q, q_dot, tau_pos);
        }
        else if(q_des.sum() == 0 && q_dot_des.sum() != 0)
        {
          // velocity controller
          kdl_comp.get_vel_PD(q_dot_des, q_dot, tau_pos);
        }
        else if(q_des.sum() != 0 && q_dot_des.sum() != 0)
        {
         // PD on position + velocity
         kdl_comp.get_traj_PD(q_des, q_dot_des, q, q_dot, tau_pos); 
        }
      }

      for (int i=0; i<ARM_DOF; i++)
      {
        temp_q[i]=arm_q[i]; // first ARM_DOF are KUKA q, next FINGER_DOF are empty for now
      }

      // compute gravity compensation w.r.t. to arm configuration:
      
      for(int i=0; i<TOTAL_FINGERS; i++)
      { 
        for(int j=0; j<FINGER_DOF; j++)
        {
          temp_q[ARM_DOF+j]=q[i*TOTAL_FINGERS+j]; // temp_q = [kuka, finger_i]
        }

        lbr4_.getGtau(i, temp_q, temp_tau_g);

        for(int j=0; j<FINGER_DOF; j++)
        {
          tau_g[i*FINGER_DOF+j]=temp_tau_g[ARM_DOF+j];
	}
      }

      // add gravity compensation
      for(int i=0;i<HAND_DOF;i++)
      {
	  tau_pos[i] += tau_g[i]; 
          tau_force[i]=tau_pos[i]-tau_g[i];
          //ROS_DEBUG("[joint %d] tau_g: %f ", i, tau_g[i]);
          //ROS_DEBUG("[joint %d] tau total: %f ", i, tau_pos[i]);
      }

      // send torques to robot:
      robot.send_torques(tau_pos);

      // update joint state topic:
      for(int i = 0; i < HAND_DOF; i++)
      {
        curr_state.position[i] = q[i];
        curr_state.velocity[i] = q_dot[i];
        curr_state.effort[i] = tau_force[i];
      }

      curr_state.header.stamp=ros::Time::now();

      st_pub.publish(curr_state);
      r.sleep();
    }
  robot.disconnect();
  return 0;
}
