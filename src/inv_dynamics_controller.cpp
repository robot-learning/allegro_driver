/*
 * Copyright (C) 2017  Balakumar Sundaralingam, LL4MA lab
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// This file connects to the allegro hand and runs an inverse dynamics controller.
#include "allegro_comms/allegro_comms.h"
#include "controllers/pd_parameters.h"
#include "controllers/kdl_controller.h"


#include <ros/ros.h>
#include <ros/console.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float32MultiArray.h>

const int NUMBER_OF_JOINTS = 16;

Eigen::VectorXd q_des, q_dot_des,q_dd_des; // initialized in main()
bool got_des=false;

void jntCallback(sensor_msgs::JointState j)
{
  vector<double> effort;
  vector<double> position;
  vector<double> velocity;
  effort=j.effort;
  position=j.position;
  velocity=j.velocity;

  q_des.setZero(NUMBER_OF_JOINTS);
  q_dot_des.setZero(NUMBER_OF_JOINTS);
  q_dd_des.setZero(NUMBER_OF_JOINTS);
  
  if(!effort.empty())
  {
    for(int i=0; i<NUMBER_OF_JOINTS; i++)
    {
      q_dd_des[i] = effort[i];
    }
  }
  if(!position.empty())
  {
    for(int i=0; i<NUMBER_OF_JOINTS; i++)
    {
      q_des[i] = position[i];
    }
  }
  if(!velocity.empty())
  {
    for(int i=0; i<NUMBER_OF_JOINTS; i++)
    {
      q_dot_des[i] = velocity[i];
    }
  }
  got_des=true;
}

int main(int argc, char** argv)
{
  ros::init(argc,argv,"allegro_inv_dynamic_node");
  ros::NodeHandle n;
  double loop_rate=1000.0;
  ros::Rate r(loop_rate);
  ros::Publisher st_pub=n.advertise<sensor_msgs::JointState>("allegro_hand_right/joint_states",1);
  ros::Subscriber cmd_sub=n.subscribe<sensor_msgs::JointState>("allegro_hand_right/joint_cmd",1,jntCallback);  

  // used for debugging by plotting desired positions and velocities
  ros::Publisher desired_state_pub=n.advertise<sensor_msgs::JointState>("allegro_hand_right/desired_joint_states",1);
  ros::Publisher p_gains_pub=n.advertise<std_msgs::Float32MultiArray>("allegro_hand_right/p_gains",1);
  ros::Publisher d_gains_pub=n.advertise<std_msgs::Float32MultiArray>("allegro_hand_right/d_gains",1);
  ros::Publisher torque_pub =n.advertise<std_msgs::Float32MultiArray>("allegro_hand_right/torque_sent",1);
  ros::Publisher gravity_pub =n.advertise<std_msgs::Float32MultiArray>("allegro_hand_right/gravity",1);

  sensor_msgs::JointState desired_state_msg;
  std_msgs::Float32MultiArray p_gains_msg;
  std_msgs::Float32MultiArray d_gains_msg;
  std_msgs::Float32MultiArray gravity_msg;
  std_msgs::Float32MultiArray torque_msg;

  for (int i=0; i<NUMBER_OF_JOINTS; i++)
  {
    torque_msg.data.push_back(-2.0);
    gravity_msg.data.push_back(-2.0);
    p_gains_msg.data.push_back(-2.0);
    d_gains_msg.data.push_back(-2.0);
  }

  // global variables
  q_des = Eigen::VectorXd::Zero(NUMBER_OF_JOINTS);
  q_dot_des = Eigen::VectorXd::Zero(NUMBER_OF_JOINTS);
  q_dd_des = Eigen::VectorXd::Zero(NUMBER_OF_JOINTS);

  Eigen::VectorXd q = Eigen::VectorXd::Zero(NUMBER_OF_JOINTS);
  Eigen::VectorXd q_dot = Eigen::VectorXd::Zero(NUMBER_OF_JOINTS);
  Eigen::VectorXd old_q = Eigen::VectorXd::Zero(NUMBER_OF_JOINTS);

  Eigen::VectorXd tau = Eigen::VectorXd::Zero(NUMBER_OF_JOINTS);
  Eigen::VectorXd tau_force = Eigen::VectorXd::Zero(NUMBER_OF_JOINTS);
  Eigen::VectorXd tau_pos = Eigen::VectorXd::Zero(NUMBER_OF_JOINTS);

  allegro_driver::allegroComms robot(0.65);
  // this vectors are determined w.r.t. TF frame of 'palm link'
  //vector<double> g_vec={0.0,-9.8,0.0}; // side grasp
  vector<double> g_vec={0.0,0.0,-9.8};// upright 
  //vector<double> g_vec={9.8,0.0,0.0}; // top grasp
  
  // create kdl controller instance:
  allegroKDL kdl_comp(g_vec,loop_rate);
  kdl_comp.load_gains(K_p,K_d,traj_K_p,traj_K_d,traj_K_i,vel_K_d,max_tau_des,max_delta_q,max_q_vel);

  Eigen::VectorXd tau_g;
  sensor_msgs::JointState curr_state;
  std::string names[]={"index_joint_0","index_joint_1","index_joint_2", "index_joint_3",
                   "middle_joint_0","middle_joint_1","middle_joint_2", "middle_joint_3",
                   "ring_joint_0","ring_joint_1","ring_joint_2", "ring_joint_3",
                   "thumb_joint_0","thumb_joint_1","thumb_joint_2", "thumb_joint_3"};
  
  
  std::vector<std::string> name_vect(names,names+sizeof(names)/sizeof(std::string)); 
  curr_state.name=name_vect;
  curr_state.position = std::vector<double>(NUMBER_OF_JOINTS);
  curr_state.velocity= std::vector<double>(NUMBER_OF_JOINTS);
  curr_state.effort = std::vector<double>(NUMBER_OF_JOINTS);

  desired_state_msg.name=name_vect;
  desired_state_msg.position = std::vector<double>(NUMBER_OF_JOINTS);
  desired_state_msg.velocity= std::vector<double>(NUMBER_OF_JOINTS);
  desired_state_msg.effort = std::vector<double>(NUMBER_OF_JOINTS);

  tau_force.resize(NUMBER_OF_JOINTS);
  bool st=robot.connect(true);

  while(ros::ok() && st)
    {
      for (int i=0; i<NUMBER_OF_JOINTS; i++)
      {
	tau_pos[i] = 0.0;
      }
      if(!robot.get_state(q,q_dot,tau))
      {
	continue;
      }
      
      
      // if to move joints
      if(got_des)
      {
	// PD on position + velocity
        //kdl_comp.get_PD(q_des, q, q_dot, tau_pos);
        //des_st_pub.publish(q_des);
        kdl_comp.get_traj_PD(q_des, q_dot_des, q, q_dot, tau_pos, p_gains_msg, d_gains_msg); 
        /**
        if(q_dot_des.sum() == 0)
        {
          // compute torques for position control
          kdl_comp.get_PD(q_des, q, q_dot, tau_pos);
        }
        else if(q_des.sum() == 0 && q_dot_des.sum() != 0)
        {
          // velocity controller
          kdl_comp.get_vel_PD(q_dot_des, q_dot, tau_pos);
        }
        else if(q_des.sum() != 0 && q_dot_des.sum() != 0) #TODO: this is not a cool condition
        {
          // PD on position + velocity
          kdl_comp.get_traj_PD(q_des, q_dot_des, q, q_dot, tau_pos); 
        }
	**/
      }

      // compute gravity compensation torques
      kdl_comp.get_G(q, tau_g);
      for(int i=0; i<NUMBER_OF_JOINTS; i++)
      {
        tau_pos[i]+=tau_g[i];  
        tau_force[i]=tau_pos[i]-tau_g[i];

	//debugging only data
        torque_msg.data[i] = tau_pos[i];
        gravity_msg.data[i] = tau_g[i];
      }

      // send torques to robot:
      robot.send_torques(tau_pos);

      p_gains_pub.publish(p_gains_msg);
      d_gains_pub.publish(d_gains_msg);
      torque_pub.publish(torque_msg);
      gravity_pub.publish(gravity_msg);

      // update joint state topic:
      Eigen::VectorXd::Map(&curr_state.position[0], q.size()) = q;
      Eigen::VectorXd::Map(&curr_state.velocity[0], q_dot.size()) = q_dot;
      Eigen::VectorXd::Map(&curr_state.effort[0], tau_force.size()) = tau_force;
       
      Eigen::VectorXd::Map(&desired_state_msg.position[0], q.size()) = q_des;
      Eigen::VectorXd::Map(&desired_state_msg.velocity[0], q_dot.size()) = q_dot_des;

      curr_state.header.stamp=ros::Time::now();
      st_pub.publish(curr_state);

      desired_state_msg.header.stamp=curr_state.header.stamp;
      desired_state_pub.publish(desired_state_msg);

      ros::spinOnce();
    }
  
  robot.disconnect();
  return 0;
}
