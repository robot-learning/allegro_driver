/*
 * Copyright (C) 2017  Balakumar Sundaralingam, LL4MA lab
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// This file connects to the allegro hand and publishes joint state to ROS:
#include "allegro_comms/allegro_comms.h"



#include <ros/ros.h>
#include <sensor_msgs/JointState.h>


int main(int argc, char** argv)
{
  
  ros::init(argc,argv,"allegro_monitor_node");
  ros::NodeHandle n;
  ros::Rate r(100);

  ros::Publisher st_pub=n.advertise<sensor_msgs::JointState>("allegro_hand_right/joint_states",1);
  
  Eigen::VectorXd q;
  Eigen::VectorXd q_dot;
  Eigen::VectorXd tau;

  allegro_driver::allegroComms robot(0.1);

  sensor_msgs::JointState curr_state;
  std::string names[]={"index_joint_0","index_joint_1","index_joint_2", "index_joint_3",
                   "middle_joint_0","middle_joint_1","middle_joint_2", "middle_joint_3",
                   "ring_joint_0","ring_joint_1","ring_joint_2", "ring_joint_3",
                   "thumb_joint_0","thumb_joint_1","thumb_joint_2", "thumb_joint_3"};
  
  
  std::vector<std::string> name_vect(names,names+sizeof(names)/sizeof(std::string)); //make it const if you can
  curr_state.name=name_vect;


  
  bool st=robot.connect(false);
  
  // pre-allocate memory for std::vector variables
  std::vector<double> std_q;
  std::vector<double> std_q_dot;
  std::vector<double> std_tau;
  robot.get_state(q,q_dot,tau);
  std_q.resize(q.size());
  std_q_dot.resize(q_dot.size());
  std_tau.resize(tau.size());

  while(ros::ok() && st)
  {
    robot.get_state(q,q_dot,tau);

    // Eigen::VectorXd -> std::vector<double> conversion
    Eigen::VectorXd::Map(&std_q[0], q.size()) = q;
    Eigen::VectorXd::Map(&std_q_dot[0], q_dot.size()) = q_dot;
    Eigen::VectorXd::Map(&std_tau[0], tau.size()) = tau;

    curr_state.position=std_q;
    curr_state.velocity=std_q_dot;
    curr_state.effort=std_tau;
    curr_state.header.stamp=ros::Time::now();
    st_pub.publish(curr_state);
  }

  if(st)
  {
    robot.disconnect();
  }
  
  
  return 0;
}
