/*
 * Copyright (C) 2017  Balakumar Sundaralingam, LL4MA lab
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// This file connects to the allegro hand and runs an inverse dynamics controller.
// Reads lbr4 joint states to compute the gravity vector

#include "allegro_comms/allegro_comms.h"
#include "controllers/kdl_controller.h"


#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <ros/package.h>
bool lbr4_state=false;
vector<double> arm_q;


// Subscribe to joint states
void stateCallback(sensor_msgs::JointState joint_msg)
{
  
  arm_q.resize(7);

  for(int i=0;i<7;++i)
    {
      arm_q[i]=joint_msg.position[i];
    }
  lbr4_state=true;
}

int main(int argc, char** argv)
{

  ros::init(argc,argv,"lbr4_allegro_grav_comp_node");
  ros::NodeHandle n;
  ros::Rate r(100);

  ros::Publisher st_pub=n.advertise<sensor_msgs::JointState>("allegro_hand_right/joint_states",1);
  ros::Subscriber lbr4_sub=n.subscribe("/lbr4/joint_states",1,stateCallback);

  vector<double> q;
  vector<double> q_dot;
  vector<double> tau;

  allegro_driver::allegroComms robot(0.25);


  // create kdl controller instance:

  //vector<double> g_vec={0.0,0.0,-18.0};// upright
  // Build kdl model:
  //  allegroKDL kdl_comp(g_vec);

  vector<double> tau_g(16,0.0);
  sensor_msgs::JointState curr_state;
  std::string names[]={"index_joint_0","index_joint_1","index_joint_2", "index_joint_3",
                   "middle_joint_0","middle_joint_1","middle_joint_2", "middle_joint_3",
                   "ring_joint_0","ring_joint_1","ring_joint_2", "ring_joint_3",
                   "thumb_joint_0","thumb_joint_1","thumb_joint_2", "thumb_joint_3"};
  
  
  std::vector<std::string> name_vect(names,names+sizeof(names)/sizeof(std::string)); //make it const if you can
  curr_state.name=name_vect;
  
  // urdf file location:
  string urdf_file = ros::package::getPath("ll4ma_robots_description");
  urdf_file.append("/robots/depreciated/lbr4_allegro_kdl.urdf");

  //urdf_file.append("/robots/lbr4_allegro_kdl.urdf");
  vector<string> ee_names={"index_tip","middle_tip","ring_tip","thumb_tip"};
  vector<string> base_names={"base_link","base_link","base_link","base_link"};
  vector<double> g_vec={0.0,0.0,-18.0};
  // initialize kdl class:
  robotKDL lbr4_(urdf_file,base_names,ee_names,g_vec);

  vector<double> hand_tau_g;
  vector<double> temp_tau_g;
  vector<double> temp_q;
  temp_q.resize(7+4);

  bool st=robot.connect(true);
  while(ros::ok() && st )
    {
     if(lbr4_state==true)
     {
      robot.get_state(q,q_dot,tau);
      for (int i=0;i<7;++i)
      {
	temp_q[i]=arm_q[i];
      }
      //cerr<<"got arm state"<<endl;
      // compute torques from controller:
      for(int i=0;i<4;i++)
      { 
	for(int j=0;j<4;j++)
	{
	  temp_q[7+j]=q[i*4+j];
	}
	temp_tau_g=lbr4_.getGtau(i,temp_q);
	for(int j=0;j<4;j++)
	{
	  tau_g[i*4+j]=temp_tau_g[7+j];
	}
      }
      //cerr<<"computed torques"<<endl;
      // send torques to robot:
      robot.send_torques(tau_g);
      
      // update joint state topic:
      curr_state.position=q;
      curr_state.velocity=q_dot;
      curr_state.effort=tau;
      curr_state.header.stamp=ros::Time::now();
      st_pub.publish(curr_state);
      } 
      ros::spinOnce();
      r.sleep();
      
    }

  robot.disconnect();
  

  
  return 0;
}
