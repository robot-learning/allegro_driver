/*
 * Copyright (C) 2017  Balakumar Sundaralingam, LL4MA lab
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "allegro_comms/allegro_comms.h"

#include <ros/console.h>

namespace allegro_driver
{
  using namespace CANAPI;
  allegroComms::allegroComms(float max_current)
  {

    got_finger_position=vector<bool>(4,false);
    // initialize variables:
    torques_to_robot=false;
    MAX_CURRENT=max_current;
    memset(&vars, 0, sizeof(vars));
    _q.resize(MAX_DOF);
    _q_old.resize(MAX_DOF);
    _q_dot.resize(MAX_DOF);
    
    _q_old_filtered.resize(MAX_DOF);
    _q_dot_old.resize(MAX_DOF);
    _q_dot_filtered.resize(MAX_DOF);
    _q_filtered.resize(MAX_DOF);
    
    /*
    _q_dot_temp.resize(MAX_DOF);
    _q_dot_smooth.resize(MAX_DOF);
    */
    _tau.resize(MAX_DOF);
    _cur_des.resize(MAX_DOF);
    _tau_des.resize(MAX_DOF);
    
    sample_rate=0.005;// sample rate for computing velocity
    write_rate=0.005;// write rate
    tstart = ros::Time::now();

    time_now=chrono::high_resolution_clock::now();
    write_time=chrono::high_resolution_clock::now();
    dt_clocks=vector<std::chrono::time_point<std::chrono::system_clock>>(4,chrono::high_resolution_clock::now());
    data_return=0;
    got_new_can_message=false;
    got_first_message=false;
  }

  bool allegroComms::connect(bool cmd_robot)
  {
    torques_to_robot=cmd_robot;
    
    CAN_Ch = GetCANChannelIndex(_T("USBBUS1"));
    printf(">CAN(%d): open\n", CAN_Ch);
    
    int ret = command_can_open(CAN_Ch);
    
    if(ret != 0)
      {
	printf("****ERROR command_canopen %d !!! \n",ret);
	return false;
      }



      
    printf("*****>CAN: starts listening CAN frames\n");
    
    printf("*****>CAN: query system id\n");

    ret = command_can_query_id(CAN_Ch);

    if(ret != 0)
      {
	printf("ERROR command_can_query_id %d !!! \n",ret);
	CANAPI::command_can_close(CAN_Ch);
	return false;
      }
    /*
    printf(">CAN: AHRS set\n");
    ret = CANAPI::command_can_AHRS_set(CAN_Ch, AHRS_RATE_100Hz, AHRS_MASK_POSE | AHRS_MASK_ACC);
    if(ret != 0)
      {
	printf("ERROR command_can_AHRS_set !!! \n");
	CANAPI::command_can_close(CAN_Ch);
	return false;
      }
    */
    printf(">CAN: system init\n");
    ret = CANAPI::command_can_sys_init(CAN_Ch, 3);//msec
    if(ret != 0)
      {
	printf("ERROR command_can_sys_init !!! \n");
	CANAPI::command_can_close(CAN_Ch);
	return false;
      }
    
    printf(">CAN: start periodic communication\n");
    ret = CANAPI::command_can_start(CAN_Ch);
    
    if(ret != 0)
      {
	printf("ERROR command_can_start !!! \n");
	CANAPI::command_can_stop(CAN_Ch);
	CANAPI::command_can_close(CAN_Ch);
	return false;
      }
    printf(">CAN: Initialized interface successfully\n");
    /*
    ioThreadRun = true;
    int ioThread_error=0;
    thr = new boost::thread(&allegroComms::ioThreadProc, this);
    //thr = new boost::thread(boost::bind(&allegroComms::ioThreadProc, this));
    //int ioThread_error = pthread_create(&hThread, NULL, &ioThreadProc, 0);
    if(ioThread_error!=0)
      {
	printf("ERROR THREAD creation failed !!! \n");
	return false;
      }

    recvNum=0;
    */
    return true;
  }
  void allegroComms::disconnect()
  {
    printf(">CAN: stop periodic communication\n");
    int ret = command_can_stop(CAN_Ch);
    if(ret < 0)
      {
	printf("ERROR command_can_stop !!! \n");
      }

    /*
    if (ioThreadRun)
      {
	printf(">CAN: stopped listening CAN frames\n");
	ioThreadRun = false;
	int status;
	thr->join();// wait for our iothread to finish
      }
    */
    printf(">CAN(%d): close\n", CAN_Ch);
    ret = command_can_close(CAN_Ch);
    if(ret < 0) printf("ERROR command_can_close !!! \n");
  }


  void allegroComms::ioThreadProc()
  {
    
    char id_des;
    char id_cmd;
    char id_src;
    int len;
    unsigned char data[8];
    
    int i;
    while (ioThreadRun)
    {
      //printf("iothreadrun\n");
      while (0 == get_message(CAN_Ch, &id_cmd, &id_src, &id_des, &len, data, FALSE))
      {
        //printf("got message\n");
        //cerr<<id_cmd<<endl;
        switch (id_cmd)
        {
          case ID_CMD_QUERY_ID:
          {
            printf(">CAN(%d): AllegroHand revision info: 0x%02x%02x\n", CAN_Ch, data[3], data[2]);
            printf("                      firmware info: 0x%02x%02x\n", data[5], data[4]);
            printf("                      hardware type: 0x%02x\n", data[7]);
          }
		break;
	      case ID_CMD_QUERY_CONTROL_DATA:
              {
                //printf("querying control data\n");
		  if (id_src >= ID_DEVICE_SUB_01 && id_src <= ID_DEVICE_SUB_04)
		    {
                      //printf("obtaining encoder values\n");
		      vars.enc_actual[(id_src-ID_DEVICE_SUB_01)*4 + 0] = (int)(data[0] | (data[1] << 8));
		      vars.enc_actual[(id_src-ID_DEVICE_SUB_01)*4 + 1] = (int)(data[2] | (data[3] << 8));
		      vars.enc_actual[(id_src-ID_DEVICE_SUB_01)*4 + 2] = (int)(data[4] | (data[5] << 8));
		      vars.enc_actual[(id_src-ID_DEVICE_SUB_01)*4 + 3] = (int)(data[6] | (data[7] << 8));
		      data_return |= (0x01 << (id_src-ID_DEVICE_SUB_01));
 		      recvNum++;
                      
                      int i_ = 4 * (id_src - ID_DEVICE_SUB_01);
                      _q[i_] = (double)(vars.enc_actual[i_]*enc_dir[i_]-32768-enc_offset[i_])
                          *(333.3/65536.0)*(3.141592/180.0);
                      i_+=1;
                      _q[i_] = (double)(vars.enc_actual[i_]*enc_dir[i_]-32768-enc_offset[i_])
                          *(333.3/65536.0)*(3.141592/180.0);
                      i_++;
                      _q[i_] = (double)(vars.enc_actual[i_]*enc_dir[i_]-32768-enc_offset[i_])
                          *(333.3/65536.0)*(3.141592/180.0);
                      i_++;
                      _q[i_] = (double)(vars.enc_actual[i_]*enc_dir[i_]-32768-enc_offset[i_])
                          *(333.3/65536.0)*(3.141592/180.0);
                      got_new_can_message=true;
		    }
		}
		break;
	      }
          }
      }
  }

 bool allegroComms::get_encoders()
 {
   char id_des;
   char id_cmd;
   char id_src;
   int len;
   unsigned char data[8];
    
   int i;
   int err=get_message(CAN_Ch, &id_cmd, &id_src, &id_des, &len, data, FALSE);
   
   while (!err)
   {
     switch (id_cmd)
     {
       case ID_CMD_QUERY_CONTROL_DATA:
       {
         //printf("querying control data\n");
         if (id_src >= ID_DEVICE_SUB_01 && id_src <= ID_DEVICE_SUB_04)
         {
           //printf("obtaining encoder values\n");
           vars.enc_actual[(id_src-ID_DEVICE_SUB_01)*4 + 0] = (int)(data[0] | (data[1] << 8));
           vars.enc_actual[(id_src-ID_DEVICE_SUB_01)*4 + 1] = (int)(data[2] | (data[3] << 8));
           vars.enc_actual[(id_src-ID_DEVICE_SUB_01)*4 + 2] = (int)(data[4] | (data[5] << 8));
           vars.enc_actual[(id_src-ID_DEVICE_SUB_01)*4 + 3] = (int)(data[6] | (data[7] << 8));
           data_return |= (0x01 << (id_src-ID_DEVICE_SUB_01));
           recvNum++;
           int i_ = 4 * (id_src - ID_DEVICE_SUB_01);
           //cerr<<id_src-ID_DEVICE_SUB_01<<endl;
           got_finger_position[id_src-ID_DEVICE_SUB_01]=true;

           _q[i_] = (double)(vars.enc_actual[i_]*enc_dir[i_]-32768-enc_offset[i_])
             *(333.3/65536.0)*(3.141592/180.0);
           i_+=1;
           _q[i_] = (double)(vars.enc_actual[i_]*enc_dir[i_]-32768-enc_offset[i_])
             *(333.3/65536.0)*(3.141592/180.0);
           i_++;
           _q[i_] = (double)(vars.enc_actual[i_]*enc_dir[i_]-32768-enc_offset[i_])
             *(333.3/65536.0)*(3.141592/180.0);
           i_++;
           _q[i_] = (double)(vars.enc_actual[i_]*enc_dir[i_]-32768-enc_offset[i_])
             *(333.3/65536.0)*(3.141592/180.0);
         }
       }
       break;

       case ID_CMD_QUERY_ID:
       {
         printf(">CAN(%d): AllegroHand revision info: 0x%02x%02x\n", CAN_Ch, data[3], data[2]);
         printf("                      firmware info: 0x%02x%02x\n", data[5], data[4]);
         printf("                      hardware type: 0x%02x\n", data[7]);
       }
       break;
     }

     err=get_message(CAN_Ch, &id_cmd, &id_src, &id_des, &len, data, FALSE);
     
   }
   return true;
 }

bool allegroComms::get_state(Eigen::VectorXd &q,Eigen::VectorXd &q_dot,Eigen::VectorXd &tau)
{
  get_encoders();
  while(std::count(got_finger_position.begin(), got_finger_position.end(), true)<4)
  {
    get_encoders();
  }

  // used for q dot estimation
  time_now = chrono::high_resolution_clock::now();

  //for each finger
  for(int i=0; i<4; ++i)
  {
    if(got_finger_position[i])
    {
      chrono::duration<double,std::nano> dur(time_now - dt_clocks[i]); 
      dt = 1e-9 * dur.count();

      // for each joint on the finger
      for (int j=0; j<4; j++)
      {
        int k = i*4+j;

	// q dot as finite difference + filter
	// filter:  40% new + 60% old
        _q_dot[k] = 0.20 * (_q[k] - _q_old[k]) / dt + 0.8 * _q_dot[k];
	_q_old[k] = _q[k];

	//return values
	q[k] = _q[k];
	q_dot[k] = _q_dot[k];
      }

      dt_clocks[i] = time_now;
      got_finger_position[i] = false;
    }
  }
      
  tau = _tau_des;
  return true;
}


void allegroComms::send_torques(const Eigen::VectorXd &tau)
{
  _tau_des=tau;
  if (data_return == (0x01 | 0x02 | 0x04 | 0x08))
  {
    // convert desired torque to desired current and PWM count
      for (int i=0; i<MAX_DOF; i++)
      {
        _cur_des[i] = _tau_des[i] * motor_dir[i];
        if (_cur_des[i] > MAX_CURRENT) _cur_des[i] = MAX_CURRENT;
        else if (_cur_des[i] < -MAX_CURRENT) _cur_des[i] = -MAX_CURRENT;
      }
      
      if(torques_to_robot)
      {
        // send torques
        for (int i=0; i<4;i++)
        {
          // the index order for motors is different from that of encoders
          switch (HAND_VERSION)
          {
            case 1:
            case 2:
              vars.pwm_demand[i*4+3] = (short)(_cur_des[i*4+0]*tau_cov_const_v2);
              vars.pwm_demand[i*4+2] = (short)(_cur_des[i*4+1]*tau_cov_const_v2);
              vars.pwm_demand[i*4+1] = (short)(_cur_des[i*4+2]*tau_cov_const_v2);
              vars.pwm_demand[i*4+0] = (short)(_cur_des[i*4+3]*tau_cov_const_v2);
              break;
                              
            case 3:
            default:
              vars.pwm_demand[i*4+3] = (short)(_cur_des[i*4+0]*tau_cov_const_v3);
              vars.pwm_demand[i*4+2] = (short)(_cur_des[i*4+1]*tau_cov_const_v3);
              vars.pwm_demand[i*4+1] = (short)(_cur_des[i*4+2]*tau_cov_const_v3);
              vars.pwm_demand[i*4+0] = (short)(_cur_des[i*4+3]*tau_cov_const_v3);
              
              break;
          }
        }
        // if better than write rate:
        for (int i=0; i<4;i++)
        {
          int ret=0;
          {
            ret=write_current(CAN_Ch, i, &vars.pwm_demand[4*i]);
          }
          if(ret!=0)
          {
            printf("Writing to hand failed with error code: %d\n",ret);
            return;
          }
        }
        data_return=0;
      }
    } 
  }

}


