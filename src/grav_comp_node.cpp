 /*
 * Copyright (C) 2017  Balakumar Sundaralingam, LL4MA lab
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// This file connects to the allegro hand and runs an inverse dynamics controller.
#include "allegro_comms/allegro_comms.h"
#include "controllers/kdl_controller.h"


#include <ros/ros.h>
#include <sensor_msgs/JointState.h>

int main(int argc, char** argv)
{
  
  ros::init(argc,argv,"allegro_grav_comp_node");
  ros::NodeHandle n;
  //ros::Rate r(50);

  ros::Publisher st_pub=n.advertise<sensor_msgs::JointState>("allegro_hand_right/joint_states",1);
  
  vector<double> q;
  vector<double> q_dot;
  vector<double> tau;

  allegro_driver::allegroComms robot(0.25);


  // create kdl controller instance:
  vector<double> g_vec={0.0,-18.0,0.0}; // in-grasp box 
  //vector<double> g_vec={0.0,0.0,-18.0};// upright
  //vector<double> g_vec={18.6,0.0,0.0}; // in-grasp box 

  allegroKDL kdl_comp(g_vec);
  vector<double> tau_g;
  tau_g.resize(16,0.0);
  sensor_msgs::JointState curr_state;
  std::string names[]={"index_joint_0","index_joint_1","index_joint_2", "index_joint_3",
                   "middle_joint_0","middle_joint_1","middle_joint_2", "middle_joint_3",
                   "ring_joint_0","ring_joint_1","ring_joint_2", "ring_joint_3",
                   "thumb_joint_0","thumb_joint_1","thumb_joint_2", "thumb_joint_3"};
  
  
  std::vector<std::string> name_vect(names,names+sizeof(names)/sizeof(std::string)); //make it const if you can
  curr_state.name=name_vect;

  
  
  bool st=robot.connect(true);
  while(ros::ok() && st)
    {
      robot.get_state(q,q_dot,tau);
      // compute torques from controller:
      tau_g=kdl_comp.get_G(q);
      // send torques to robot:
      robot.send_torques(tau_g);

      // update joint state topic:
      curr_state.position=q;
      curr_state.velocity=q_dot;
      curr_state.effort=tau;
      curr_state.header.stamp=ros::Time::now();
      st_pub.publish(curr_state);
      ros::spinOnce();
      //r.sleep();
      
    }
  
  robot.disconnect();
  

  
  return 0;
}
